package ru.inordic.alexeybaluev.lesson;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Slova {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите 2 строки");
//        String s1 = scanner.nextLine();
//        String s2 = scanner.nextLine();

        String s1 = "     нов!        нов!!! !!нов      !!!нов         нопр опркв олпваоп                 па      ";
        String s2 = "     нов   онов орпол олдаво ирекир  нопр  шгнш   нов нов      нов         нопр опркрпв олпваопп      поа      ";

        String s11 = removeZnaki(s1);
        String s22 = removeZnaki(s2);


        Set<String> a1 = getSet(s11);
        Set<String> a2 = getSet(s22);
        Set<String> sovpad = new HashSet<>(a1);
        sovpad.retainAll(a2);
        System.out.println("совпадающие слова в строках");
        for (String s : sovpad) {
            System.out.print(s + " ");
        }
    }

    public static String removeZnaki(String s) {
        char[] c = s.toCharArray();
        for (int i = 0; i < c.length; i++) {
            if (!Character.isLetter(c[i]) && !Character.isWhitespace(c[i]))
                c[i] = ' ';
        }
        String onlyAbc = new String(c);
        return onlyAbc;
    }


    public static Set<String> getSet(String s) {
        String after = s.trim().replaceAll(" +", " ");
        String[] array = after.split(" ");
        Set<String> strings = new HashSet<>(Arrays.asList(array));
        return strings;

//        Set<String> strings = new HashSet<>();
//        for (String a : array) {
//            strings.add(a);
//        }

    }


}

